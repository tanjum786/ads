function init() {
  window.dynamicContent = document.getElementById("content"); // The div to replace the image into.
  window.clickUrl = undefined; // the global variable to store the url from mapping. 'undefined' during init process.
  window.companyName = undefined; // the global variable to store the company name. 'undefined' during init process.
  window.brandLogo = document.getElementById("logo");
  window.logoUrl = "";
  // brand mapping
  window.brandMappings = {
    "Activision Blizzard, Inc.": "Activision Blizzard.svg",
    "Airbnb, Inc.": "Airbnb.svg",
    "Altair Engineering": "Altair.svg",
    "AVI - SPL": "AVI-SPL.svg",
    Alteryx: "Alteryx.svg",
    AppDynamics: "AppDynamics.svg",
    "Applied Systems": "Applied Systems.svg",
    "advanceautoparts.com": "Advance Auto Parts",
    "Bottomline Technologies": "Bottomline.svg",
    "Bill.com": "bill.svg",
    Calix: "Calix.svg",
    "CCC Information Services":"CCC.png",
    "CDS Global": "CDS Global.svg",
    "Credit Karma": "Credit Karma.svg",
    "Merrill Corporation": "Datasite.svg",
    "Electronic Arts, Inc.":"Electronic Arts.svg",
   "DoorDash, Inc.": "Doordash.svg",
    Enverus: "Enverus.svg",
   "Facebook, Inc.": "Facebook.svg",
    "GoDaddy, Inc.": "GoDaddy.svg",
    Imperva: "Imperva.svg",
    Instructure: "Instructure.svg",
    KGPCo: "KGPCo.svg",
    "Lumen Technologies (fmr CenturyLink)": "Lumen.svg",
   "Lyft, Inc.": "Lyft.svg",
    Mindbody: "Mindbody.svg",
   "Netsmart Technologies": "Netsmart.svg",
    "Nextgen Healthcare": "Nextgen Healthcare.svg",
    "Nutanix, Inc.": "Nutanix.svg",
    Paylocity: "Paylocity.svg",
   "PitchBook Data": "PitchBook.svg",
    PowerSchool: "PowerSchool.svg",
    "Procore Technologies": "Procore.svg",
    "PSG Global Solutions": "PSG Global Solutions.svg",
    Q2: "Q2.svg",
    Qualtrics: "Qualtrics.svg",
    "Serials Solutions": "Serials Solutions.svg",
    TaskUs: "TaskUs.svg",
    "Rally Health":"Rally Health.svg",
    Technosoft: "Technosoft.svg",
    "Diligent":"Diligent.svg",
    Techwave: "Techwave.svg",
    "Teletrac Navman": "Teletrac Navman.svg",
   "Thryv Holdings, Inc. ": "Thryv.svg",
    TiVo: "TiVo.svg",
    Tripwire: "Tripwire.svg",
    Upwork: "Upwork.svg",
    Xoriant: "Xoriant.svg",
    ZoomInfo: "ZoomInfo.svg",
    AspenTech: "AspenTech.svg",
    "Global Payments, Inc.": "Global Payments.svg",
    HubSpot: "HubSpot.svg",
   "Hyland Software": "Hyland.svg",
    "Inmar": "Inmar Intelligence.svg",
   "Leidos Holdings, Inc.": "Leidos.svg",
    "McAfee Corp.": "McAfee.svg",
    "Oracle Corporation": "Oracle.svg",
    Roblox: "Roblox.svg",
    "SS&C Technologies Holding, Inc.": "SS_C Technologies.svg",
    "Take-Two Interactive Software Inc. ": "Take-Two.svg",
    MathWorks: "MathWorks.svg",
    "Total System Services, Inc.": "TSYS.svg",
    "Zynga, Inc.": "Zynga.svg",
    "3d Systems": "3d Systems.svg",
    Altair: "Altair.svg",
    Availity: "Availity.png",
    Bottomline: "Bottomline.svg",
    Brillio: "Brillio.svg",
    AspenTech: "AspenTech.svg",
    "Cars.com": "car.com.png",
    CNSI: "CNSI.svg",
    "Community Brands": "Community Brands.svg",
    Curvature: "Curvature.svg",
    "Dialog Solutions": "Dialog Solutions.svg",
    Elavon: "Elavon.svg",
    "Electronic Arts": "Electronic Arts.svg",
    eToro: "eToro.svg",
    "FIS": "FIS Global.svg",
    "Infor, Inc.": "Infor.svg",
    Kontron: "Kontron.svg",
    Proofpoint: "Proofpoint.svg",
    Zynga: "Zynga.svg",
   "RealPage, Inc.": "RealPage.svg",
    "ResMed, Inc.": "ResMed.svg",
    Squarespace: "Squarespace.svg",
    "Synaptics, Inc.": "Synaptics.svg",
    "TradeStation Group": "TradeStation.svg",
    "West Monroe Partners": "West Monroe Partners.svg",
    "Zensar Technologies": "Zensar.svg",
    Granicus: "Granicus.svg",
    "Greenway Health": "Greenway Health.svg",
    Harmonic: "Harmonic.svg",
    ProQuest: "ProQuest.svg",
    Shopify: "Shopify.svg",
    "Sony Interactive Entertainment": "Sony Interactive Entertainment.svg",
    "Trend Micro Inc.": "Trend Micro.svg",
    "WEX, Inc.": "Wex.svg",
    "AVI-SPL": "AVI-SPL.svg",
    "Cornerstone OnDemand": "Cornerstone OnDemand.svg",
    "Red Hat": "Red Hat.svg",
    "Square, Inc.": "Square.svg",
    "Twilio, Inc.": "Twilio.svg",
    "Twitter, Inc.": "Twitter.svg",
    Yelp: "Yelp.svg",
  };

  // Showing loader
  showImage(loader);
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.addEventListener("readystatechange", function() {
    addListeners(); // Adding click listener to content; to handle click redirects
    if (this.readyState == 4) {
      // After we get the response.
      showImage();
      if (this.status === 200) {
        // if we get the success response
        res = JSON.parse(this.responseText); // this is how you parse the response from company details api
        companyName = res.company.name;
        showLogo(companyName);
        var matchedMappingKey = Object.keys(companyUrlMapping).find(function(
          name
        ) {
          return name.toLowerCase() === companyName.toLowerCase();
        }); // match company name ignoring case from the mapping object
        if (companyName && matchedMappingKey) {
          clickUrl = companyUrlMapping[matchedMappingKey]; // get the custom click url from mapping
          displayPersonalizedText(companyName); // passing the company name to add personalized company name text on top of the ad
        }
      }
    }
  });
  xhr.open("GET", "https://epsilon.6sense.com/v1/company/details"); // The actual call made to company details api
  xhr.setRequestHeader(
    "Authorization",
    "Token ae0a8d2658ac4a808f3c1d0e63139b8b1c9cf127"
  ); // The company details api-token (provided by 6sense) goes here.
  // xhr.setRequestHeader("X-Forwarded-For", "<IP ADDRESS/>"); // To test your HTML5 ad uncomment this line and put the appropiate ip address for the account you want to test
  xhr.timeout = 5000; // ideal to wait for 5 seconds
  xhr.send(); // The call starts here.
}

function showImage() {
  dynamicContent.style.backgroundImage = "url(" + imageUrl + ")"; // this shows your creative
}
function displayPersonalizedText(userCompanyName) {
  var companyNameEle = document.getElementById("company-name"); // the div that shows the custom text, the company name

  // companyNameEle.innerHTML = userCompanyName; // move the text a little more down to accomodate showing company name
}

function showLogo(mapping) {
  var matchedbrand = Object.keys(brandMappings).find(function(name) {
    return name.toLowerCase() === mapping.toLowerCase();
  });

  if (mapping && matchedbrand) {
    logoUrl = brandMappings[matchedbrand];
    brandLogo.style.backgroundImage = "url(" + '"./Logo/' + logoUrl + '"' + ")";
  } else {
    brandLogo.style.display = "none";
    showImage(noMatch);
  }
}

function addListeners() {
  dynamicContent.addEventListener("click", clickEventHandler, false);
}

function clickEventHandler(e) {
  // function that redirects to landing page url. We get the landing page url from APPNEXUS library.
  function updateClickUrl(clickTag, dynamicClickUrl, dynamicCompanyName) {
    var baseClickTag, anClickUrl, sixSenseClickUrl, modifiedDynamicClickUrl;
    var anClickUrlKey = "clickenc=";
    modifiedDynamicClickUrl = new URL(dynamicClickUrl);
    modifiedDynamicClickUrl.searchParams.set("xsrc", "6s"); // set param for source to be from 6s to the clickurl from mapped item
    // add your query params to your custom click URL here, if needed
    if (dynamicCompanyName) {
      modifiedDynamicClickUrl.searchParams.set("xdcname", dynamicCompanyName); // set param to recogonise the user company name to the clickUrl from the maped item
    }
    baseClickTag = clickTag.split(anClickUrlKey)[0];
    anClickUrl = clickTag.split(anClickUrlKey)[1];
    sixSenseClickUrl = new URL(decodeURIComponent(anClickUrl));
    sixSenseClickUrl.searchParams.set("redirect", modifiedDynamicClickUrl); // update redirect URL to clickUrl from mapped item with, modified with additionam params 'xsrc' and 'xdcname'
    // DO NOT ADD ANY MORE QUERY PARAMS HERE, this is for 6sense analytics
    if (dynamicCompanyName) {
      sixSenseClickUrl.searchParams.set("xdcname", dynamicCompanyName); // set param to recogonise the user company name
    }
    return baseClickTag + anClickUrlKey + encodeURIComponent(sixSenseClickUrl);
  }

  var clickTag = APPNEXUS.getClickTag();
  if (window.clickUrl) {
    // if custom click url for the matched mapping item present then update the app nexus
    // click tag with some more custom query params
    // clickTag = updateClickUrl(clickTag, window.clickUrl, window.companyName);
  }
  window.open(clickTag);
}

function init() {
  window.dynamicContent = document.getElementById("content"); // The div to replace the image into.
  window.clickUrl = undefined; // the global variable to store the url from mapping. 'undefined' during init process.
  window.companyName = undefined; // the global variable to store the company name. 'undefined' during init process.
  window.brandLogo = document.getElementById("logo");
  window.defaultLogo = "Global_1.png";
  window.logoUrl = "";
  // brand mapping
  window.brandMappings = {
    "Activision Blizzard": "Activision Blizzard.svg",
    "Airbnb": "Airbnb.svg",
    "Alteryx": "Alteryx.svg",
    "AppDynamics": "AppDynamics.png",
    "Applied Systems": "Applied Systems.svg",
    "advanceautoparts.com": "Advance Auto Parts",
    "Bill.com": "bill.svg",
    "Calix": "Calix.svg",
    "CDS Global": "CDS Global.svg",
    "Credit Karma": "Credit Karma.svg",
    "Datasite": "Datasite.svg",
    "DoorDash": "Doordash.svg",
    "Enverus": "Enverus.svg",
    "Facebook": "Facebook.svg",
    "GoDaddy": "GoDaddy.svg",
    "Imperva": "Imperva.svg",
    "Instructure": "Instructure.svg",
    "KGPCo": "KGPCo.svg",
    "Lumen": "Lumen.svg",
    "Lyft": "Lyft.svg",
    "Mindbody": "Mindbody.svg",
    "Netsmart": "Netsmart.svg",
    "Nextgen Healthcare": "Nextgen Healthcare.svg",
    "Nutanix": "Nutanix.svg",
    "Paylocity": "Paylocity.svg",
    "PitchBook": "PitchBook.svg",
    "PowerSchool": "PowerSchool.svg",
    "Procore": "Procore.svg",
    "PSG Global Solutions": "PSG Global Solutions.png",
    "Q2": "Q2.svg",
    "Qualtrics": "Qualtrics.svg",
    "Serials Solutions": "Serials Solutions.svg",
    "TaskUs": "TaskUs.svg",
    "Technosoft": "Technosoft.png",
    "Techwave": "Techwave.svg",
    "Teletrac Navman": "Teletrac Navman.svg",
    "Thryv": "Thryv.svg",
    "TiVo": "TiVo.svg",
    "Tripwire": "Tripwire.svg",
    "Upwork": "Upwork.svg",
    "Xoriant": "Xoriant.svg",
    "ZoomInfo": "ZoomInfo.svg",
    "AspenTech": "AspenTech.svg",
    "Global Payments": "Global Payments.svg",
    "HubSpot": "HubSpot.svg",
    "Hyland": "Hyland.svg",
    "Inmar Intelligence": "Inmar Intelligence.svg",
    "Leidos": "Leidos.svg",
    "McAfee": "McAfee.svg",
    "Oracle": "Oracle.svg",
    "Roblox": "Roblox.svg",
    "SS&C Technologies": "SS_C Technologies.svg",
    "Take-Two": "Take-Two.svg",
    "MathWorks": "MathWorks.svg",
    "TSYS": "TSYS.svg",
    "Zynga": "Zynga.svg",
    "3d Systems": "3d Systems.svg",
    "Altair": "Altair.svg",
    "Availity": "Availity.png",
    "Bottomline": "Bottomline.svg",
    "Brillio": "Brillio.svg",
    "AspenTech": "AspenTech.svg",
    "Cars.com": "car.com.svg",
    "CNSI": "CNSI.svg",
    "Community Brands": "Community Brands.svg",
    "Curvature": "Curvature.svg",
    "Dialog Solutions": "Dialog Solutions.svg",
    "Elavon": "Elavon.svg",
    "Electronic Arts": "Electronic Arts.svg",
    "eToro": "eToro.svg",
    "FIS Global": "FIS Global.svg",
    "Infor": "Infor.svg",
    "Kontron": "Kontron.svg",
    "Proofpoint": "Proofpoint.svg",
    "Zynga": "Zynga.svg",
    "RealPage": "RealPage.svg",
    "ResMed": "ResMed.svg",
    "Squarespace": "Squarespace.svg",
    "Synaptics": "Synaptics.svg",
    "TradeStation": "TradeStation.svg",
    "West Monroe Partners": "West Monroe Partners.svg",
    "Zensar": "Zensar.svg",
    "CCC": "CCC.svg",
    "Granicus": "Granicus.svg",
    "Greenway Health": "Greenway Health.svg",
    "Harmonic": "Harmonic.svg",
    "ProQuest": "ProQuest.svg",
    "Shopify": "Shopify.svg",
    "Sony Interactive Entertainment": "Sony Interactive Entertainment.svg",
    "Trend Micro": "Trend Micro.svg",
    "Wex": "Wex.svg",
    "AVI-SPL": "AVI-SPL.svg",
    "Cornerstone": "Cornerstone OnDemand.svg",
    "Red Hat": "Red Hat.svg",
    "Square": "Square.svg",
    "Twilio": "Twilio.svg",
    "Twitter": "Twitter.svg",
    "Yelp": "Yelp.svg",
  };

  // Showing loader
  showImage(loader);
  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;
  xhr.addEventListener("readystatechange", function() {
    addListeners(); // Adding click listener to content; to handle click redirects
    if (this.readyState == 4) {
      // After we get the response.
      showImage();
      //calling logo function manually for testing
      //    showLogo('Volkswagen Group Of America');
      if (this.status === 200) {
        // if we get the success response
        res = JSON.parse(this.responseText); // this is how you parse the response from company details api
        companyName = res.company.name;
        showLogo(companyName);
        var matchedMappingKey = Object.keys(companyUrlMapping).find(function(
          name
        ) {
          return name.toLowerCase() === companyName.toLowerCase();
        }); // match company name ignoring case from the mapping object
console.log(matchedMappingKey,"s");
        if (companyName && matchedMappingKey) {
          clickUrl = companyUrlMapping[matchedMappingKey]; // get the custom click url from mapping
          //   displayPersonalizedText(companyName); // passing the company name to add personalized company name text on top of the ad
        }
      }
    }
  });
  xhr.open("GET", "https://epsilon.6sense.com/v1/company/details"); // The actual call made to company details api
  xhr.setRequestHeader(
    "Authorization",
    "Token ae0a8d2658ac4a808f3c1d0e63139b8b1c9cf127"
  ); // The company details api-token (provided by 6sense) goes here.
  xhr.setRequestHeader("X-Forwarded-For", "174.108.116.153"); // To test your HTML5 ad uncomment this line and put the appropiate ip address for the account you want to test
  xhr.timeout = 5000; // ideal to wait for 5 seconds
  xhr.send(); // The call starts here.
}

function showImage() {
  dynamicContent.style.backgroundImage = "url(" + imageUrl + ")"; // this shows your creative
}

function showLogo(mapping) {
  var matchedbrand = Object.keys(brandMappings).find(function(name) {
    return name.toLowerCase() === mapping.toLowerCase();
  });
  

  if (mapping && matchedbrand) {
    logoUrl = brandMappings[matchedbrand];
    console.log(logoUrl,"d");
    brandLogo.style.backgroundImage = "url(" + '"logo/' + logoUrl + '"' + ")";
  } else {
    brandLogo.style.display = "none";
    showImage(noMatch);
  }
}
function displayPersonalizedText(userCompanyName) {
  var companyNameEle = document.getElementById("company-name"); // the div that shows the custom text, the company name
  companyNameEle.innerHTML = userCompanyName + ",";
  document.getElementById("text-area").style.top = "30%"; // move the text a little more down to accomodate showing company name
}

function addListeners() {
  dynamicContent.addEventListener("click", clickEventHandler, false);
}

function clickEventHandler(e) {
  // function that redirects to landing page url. We get the landing page url from APPNEXUS library.
  function updateClickUrl(clickTag, dynamicClickUrl, dynamicCompanyName) {
    var baseClickTag, anClickUrl, sixSenseClickUrl, modifiedDynamicClickUrl;
    var anClickUrlKey = "clickenc=";
    modifiedDynamicClickUrl = new URL(dynamicClickUrl);
    modifiedDynamicClickUrl.searchParams.set("xsrc", "6s"); // set param for source to be from 6s to the clickurl from mapped item
    // add your query params to your custom click URL here, if needed
    if (dynamicCompanyName) {
      modifiedDynamicClickUrl.searchParams.set("xdcname", dynamicCompanyName); // set param to recogonise the user company name to the clickUrl from the maped item
    }
    baseClickTag = clickTag.split(anClickUrlKey)[0];
    anClickUrl = clickTag.split(anClickUrlKey)[1];
    sixSenseClickUrl = new URL(decodeURIComponent(anClickUrl));
    sixSenseClickUrl.searchParams.set("redirect", modifiedDynamicClickUrl); // update redirect URL to clickUrl from mapped item with, modified with additionam params 'xsrc' and 'xdcname'
    // DO NOT ADD ANY MORE QUERY PARAMS HERE, this is for 6sense analytics
    if (dynamicCompanyName) {
      sixSenseClickUrl.searchParams.set("xdcname", dynamicCompanyName); // set param to recogonise the user company name
    }
    return baseClickTag + anClickUrlKey + encodeURIComponent(sixSenseClickUrl);
  }

  var clickTag = APPNEXUS.getClickTag();
  if (window.clickUrl) {
    // if custom click url for the matched mapping item present then update the app nexus
    // click tag with some more custom query params
    clickTag = updateClickUrl(clickTag, window.clickUrl, window.companyName);
  }
  window.open(clickTag);
}
